#include "gtest/gtest.h"
#include "day-of-year.hpp"

struct DayOfYearTestSuite {};

TEST(DayOfYearTestSuite, dummyTest)
{
  ASSERT_TRUE(true);
}

TEST(DayOfYearTestSuite, January1stIsFitstDayOfYear)
{
  ASSERT_EQ(dayOfYear(1, 1, 2020), 1);
}

TEST(DayOfYearTestSuite, LeapYearTest)
{
  ASSERT_EQ(dayOfYear(3, 1, 2020), 61);
}

TEST(DayOfYearTestSuite, February29thIs60thDayOfLeapYear)
{
  ASSERT_EQ(dayOfYear(2, 29, 2020), 60);
}

TEST(DayOfYearTestSuite, March1stIs61thDayOfLeapYear)
{
  ASSERT_EQ(dayOfYear(3, 1, 2020), 61);
}

TEST(DayOfYearTestSuite, March1stIs60thDayOfLeapYear)
{
  ASSERT_EQ(dayOfYear(3, 1, 2021), 60);
}