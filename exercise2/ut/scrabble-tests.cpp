#include "gtest/gtest.h"
#include "scrabble.hpp"

struct ScrabbleTestSuite {};

TEST(ScrabbleTestSuite, dummyTest)
{
  ASSERT_TRUE(true);
}

TEST(ScrabbleTestSuite, aIs1)
{
  ASSERT_EQ(countPoints("a"), 1);
}

TEST(ScrabbleTestSuite, lowerAndUpperCaseWord)
{
  ASSERT_EQ(countPoints("pAro"), 6);
}

TEST(ScrabbleTestSuite, dashInWord)
{
  ASSERT_EQ(countPoints("ice-cream"), 14);
}