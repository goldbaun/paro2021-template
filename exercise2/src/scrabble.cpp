#include "scrabble.hpp"

int countPoints(std::string word)
{
    int points = 0;
    for (auto i : word)
    {
        char tmp = std::tolower(i);
        if (tmp == 'a' || tmp == 'e' || tmp == 'i' || tmp == 'o' || tmp == 'u' || tmp == 'l' || tmp == 'n' || tmp == 'r' || tmp == 's' || tmp == 't')
        {
            points += 1;
        }
        if (tmp == 'd' || tmp == 'g')
        {
            points += 2;
        }
        if (tmp == 'b' || tmp == 'c' || tmp == 'm' || tmp == 'p')
        {
            points += 3;
        }
        if (tmp == 'f' || tmp == 'h' || tmp == 'v' || tmp == 'w' || tmp == 'y')
        {
            points += 4;
        }
        if (tmp == 'k')
        {
            points += 5;
        }
        if (tmp == 'j' || tmp == 'x')
        {
            points += 8;
        }
        if (tmp == 'q' || tmp == 'z')
        {
            points += 10;
        }
    }
    
    return points;
}